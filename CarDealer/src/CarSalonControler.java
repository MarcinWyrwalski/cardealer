import pl.Chomik.CarEquipmentTypes.CarBodyType;
import pl.Chomik.CarEquipmentTypes.CarColor;
import pl.Chomik.CarEquipmentTypes.CarEngineType;
import pl.Chomik.CarEquipmentTypes.CarUpholstery;

import java.util.EnumSet;
import java.util.Scanner;

class CarSalonControler {
    private double amountOfMoney = 0;

    //setup car to work with
    private CarBulider.Bulider car = new CarBulider.Bulider(null);

    public CarModels displayCars(double amountOfMoneyAvalible) {

        if (!isAmountOfMoneySufficient(amountOfMoneyAvalible)) {
            System.out.println(" Przykro mi ale nie masz wystarczajacych funduszy. ");
            System.exit(9);
        }

        displayAvalibleEquipment(amountOfMoneyAvalible);
        // it have to be a method to get numbers of stream - then if 0 -> if wount be neseesary.

        CarModels car = carSelector(amountOfMoneyAvalible);

        return car;
    }

    private void displayAvalibleEquipment(double amountOfMoneyAvalible) {
        CarModels.stream()
                .filter(carModels -> carModels.getCarPrice() < amountOfMoneyAvalible)
                .forEach(System.out::println);
    }

    private boolean isAmountOfMoneySufficient(double amountOfMoneyAvalible) {

        for (CarModels CarModels : EnumSet.allOf(CarModels.class)) {
            if (CarModels.carPrice < amountOfMoneyAvalible) {
                return true;
            }
        }
        return false;
    }

    private CarModels carSelector(double amountOfMoneyAvalible) {

        Scanner sc = getScanner();
        System.out.println("Wybierz samochód: ");
        String carSelector = sc.nextLine().toUpperCase();

        if (carSelectorChecker(carSelector)) {
            car = new CarBulider.Bulider(CarModels.valueOf(carSelector.toUpperCase()));
            amountOfMoney = amountOfMoney - CarModels.valueOf(carSelector).getCarPrice();
            return null;
        } else {
            carSelector(amountOfMoney);
        }
        return null;
    }

    private boolean carSelectorChecker(String carSelector) {
        for (CarModels cet : CarModels.values()
        ) {
            if (cet.name().equals(carSelector.toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    private void CarColourSelector(double amountOfMoney) {
        double amouuntOFMoney;
        CarColor.stream()
                .filter(carColor -> carColor.carColourPrice < amountOfMoney)
                .forEach(System.out::println);

        Scanner sc = getScanner();
        System.out.println("wybrierz kolor samochodu");
        String selector = sc.nextLine().toUpperCase();

        if (car.getCarColor() != null) amouuntOFMoney = amountOfMoney + getCurrentEngineTypePrice(car);

        if (carColourType(selector)) {
            setupCarColor(CarColor.valueOf(selector));
        } else {
            CarColourSelector(amountOfMoney);
        }

        amouuntOFMoney = amountOfMoney - CarColor.valueOf(selector).getCarColourPrice();

        displayEquipmentSeector(amouuntOFMoney);
    }

    private boolean carColourType(String str) {

        for (CarColor cet : CarColor.values()
        ) {
            if (cet.name().equals(str)) {
                return true;
            }
        }
        return false;
    }

    private void setupCarColor(CarColor carColor2) {
        car.setupCarColor(carColor2);
    }

    void start() {

        amountOfMoney = getClientFunds();
        displayCars(amountOfMoney);
        displayEquipmentSeector(amountOfMoney);
    }

    private void displayEquipmentSeector(double amouuntOFMoney) {
//        System.out.println(car);

        Scanner sc = getScanner();
        System.out.println("Wybór wyposarzenia: ");
        CarSetup.stream()
                .forEach(System.out::println);

        String selector = sc.nextLine().toUpperCase();

        if (displayEqupimentSelectorCheck(selector)) {

            switch (CarSetup.valueOf(selector.toUpperCase())) {

                case COLOR:
                    CarColourSelector(amouuntOFMoney);
                    break;
                case BODY_STYLE:
                    CarBodySelector(amouuntOFMoney);
                    break;
                case UPHOLSTERY:
                    CarUpholsterySelector(amouuntOFMoney);
                    break;
                case ENGINE_TYPE:
                    CarEngineTypeSelector(amouuntOFMoney);
                    break;
                case DISPLAY:

                    // IF CUSTOMER DID`T SELECT ALL OPTION - DEAFAULT WILL BE SETUP.
                    if (car.getCarEngineType() == null) {
                        car.setupCarEngineType(CarEngineType.DIESEL);
                    }
                    if (car.getCarUpholstery() == null) {
                        car.setupCarUpholseryType(CarUpholstery.PLASTIC);
                    }
                    if (car.getCarBodyType() == null) {
                        car.setupCarBodyType(CarBodyType.SEDAN);
                    }
                    if (car.getCarColor() == null) {
                        car.setupCarColor(CarColor.SNOWY_WHITE);
                    }

                    car.dispalyCurrentConfiguration(amouuntOFMoney);
                    displayEquipmentSeector(amouuntOFMoney);
                    break;
                case FINISH:
                    System.out.println("printing invoice");
                    // add car creation with set up parameters and write it to db / sell, servis parts/
                    System.out.println(car);
                    System.exit(0);
            }
        } else {
            System.out.println("Błędny wybór");
            displayEquipmentSeector(amouuntOFMoney);
        }
    }

    private boolean displayEqupimentSelectorCheck(String selector) {
        for (CarSetup cet : CarSetup.values()
        ) {
            if (cet.name().equals(selector.toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    private void CarEngineTypeSelector(double amouuntOFMoney) {
        double finalAmouuntOFMoney = amouuntOFMoney;
        CarEngineType.stream()
                .filter(carEngineType -> carEngineType.getCarEngineTypePrice() < finalAmouuntOFMoney)
                .forEach(System.out::println);

        Scanner sc = getScanner();
        System.out.println("Podaj typ silnika: ");
        String selector = sc.nextLine().toUpperCase();

        if (car.getCarEngineType() != null) {
            amouuntOFMoney = amountOfMoney + getCurrentEngineTypePrice(car);
        } else amouuntOFMoney = amountOfMoney;

        if (getValueOfCarEngineType(selector)) {
            selectEngineType(selector);
        } else {
            System.out.println("Blędna wartość!");
            CarEngineTypeSelector(amouuntOFMoney);
        }

        amouuntOFMoney = amountOfMoney - CarEngineType.valueOf(selector).getCarEngineTypePrice();

        displayEquipmentSeector(amouuntOFMoney);
    }

    private int getCurrentEngineTypePrice(CarBulider.Bulider car) {
        return car.getCarEngineType().getCarEngineTypePrice();
    }

    private void selectEngineType(String selector) {
        car.setupCarEngineType(CarEngineType.valueOf(selector));
    }

    private void CarUpholsterySelector(double amouuntOFMoney) {
        double finalAmouuntOFMoney = amouuntOFMoney;
        CarUpholstery.stream()
                .filter(carUpholstery -> carUpholstery.getCarUpholsteryProce() < finalAmouuntOFMoney)
                .forEach(System.out::println);

        Scanner sc = getScanner();
        System.out.println("Podaj rodzaj tapicerki: ");
        String selector = sc.nextLine().toUpperCase();

        if (car.getCarUpholstery() != null) {
            amouuntOFMoney = amountOfMoney + getCurrentUpholstery(car);
        } else amouuntOFMoney = amountOfMoney;

        if (getValueOfCarUpholstery(selector)) {
            selectUpholserType(selector);
        } else {
            CarUpholsterySelector(amouuntOFMoney);
        }

        amouuntOFMoney = amountOfMoney - CarUpholstery.valueOf(selector).getCarUpholsteryProce();

        displayEquipmentSeector(amouuntOFMoney);
    }

    private boolean getValueOfCarUpholstery(String str) {

        for (CarUpholstery cet : CarUpholstery.values()
        ) {
            if (cet.name().equals(str)) {
                return true;
            }
        }
        return false;
    }

    private double getCurrentUpholstery(CarBulider.Bulider car) {
        return car.getCarUpholstery().getCarUpholsteryProce();
    }

    private void selectUpholserType(String selector) {
        car.setupCarUpholseryType(CarUpholstery.valueOf(selector));
    }

    private void CarBodySelector(double amouuntOFMoney) {

        double finalAmouuntOFMoney = amouuntOFMoney;
        CarBodyType.stream()
                .filter(carBodyType -> carBodyType.getCarBodyTypePrice() < finalAmouuntOFMoney)
                .forEach(System.out::println);

        Scanner sc = getScanner();
        System.out.println("Podaj typ nadwozia: ");
        String selector = sc.nextLine().toUpperCase();

        CarBodyType carBody = CarBodyType.lookup(selector);
        selectBodyType(carBody);

        if (car.getCarBodyType() != null) {
            amouuntOFMoney = amountOfMoney + getCurrentBodyType(car);
        } else amouuntOFMoney = amountOfMoney;

        if (CarBodyTypeEnumCheck(selector)) {
            selectBodyType(carBody);
        } else {
            CarBodySelector(amouuntOFMoney);
        }
        amouuntOFMoney = amountOfMoney - CarBodyType.valueOf(selector).getCarBodyTypePrice();

        displayEquipmentSeector(amouuntOFMoney);
        selectBodyType(carBody);

        displayEquipmentSeector(amouuntOFMoney);
    }

    private boolean CarBodyTypeEnumCheck(String str) {
        for (CarBodyType cet : CarBodyType.values()
        ) {
            if (cet.name().equals(str)) {
                return true;
            }
        }
        return false;
    }

    private double getCurrentBodyType(CarBulider.Bulider car) {
        return car.getCarBodyType().getCarBodyTypePrice();
    }

    private void selectBodyType(CarBodyType selector) {
        car.setupCarBodyType(selector);
    }

    private int getClientFunds() {
        Scanner sc = getScanner();
        System.out.println("Proszę podać kwotę jaką klient dysponuje? ");
        int amountOfClientMoney = sc.nextInt();
        sc.nextLine();
        return amountOfClientMoney;
    }

    private Scanner getScanner() {
        return new Scanner(System.in);
    }

    private boolean getValueOfCarEngineType(String str) {
        for (CarEngineType cet : CarEngineType.values()
        ) {
            if (cet.name().equals(str)) {
                return true;
            }
        }
        return false;
    }
}
