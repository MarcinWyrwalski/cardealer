package pl.Chomik.CarEquipmentTypes;

import java.util.stream.Stream;

public enum CarBodyType {

    SEDAN(0),
    KOMBI(1500),
    PICK_UP(5000),
    ROADSTER(7500),
    CABRIOLET(10000);


    public int carBodyTypePrice;

    CarBodyType (int carBodyTypePrice) {this.carBodyTypePrice = carBodyTypePrice;}

    public static Stream<CarBodyType> stream(){return Stream.of(CarBodyType.values());}

    public int getCarBodyTypePrice () {return carBodyTypePrice;}

    public static CarBodyType lookup(String str){
        try {
            return CarBodyType.valueOf(str);
        } catch (IllegalArgumentException e){
            System.out.println("Błędna wartośc! ustawiono wartość domyślną!");
        } return null;
    }

}
