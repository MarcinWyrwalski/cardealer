package pl.Chomik.CarEquipmentTypes;

import java.util.stream.Stream;

public enum CarEngineType {

    DIESEL(0),
    LPG(2500),
    BENZIN(5500),
    HYBIRD(7900),
    AA(12500),
    UNOBTANIUM(99900);

    private int carEngineTypePrice;

    CarEngineType(int carEngineTypePrice) {
        this.carEngineTypePrice = carEngineTypePrice;
    }

    public static Stream<CarEngineType> stream(){
        return Stream.of(CarEngineType.values());
    }

    public int getCarEngineTypePrice(){
        return carEngineTypePrice;
    }


}
