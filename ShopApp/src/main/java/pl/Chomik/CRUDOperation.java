package pl.Chomik;

public enum CRUDOperation {

    CREATE,
    READ,
    UPDATE,
    DELETE
}
