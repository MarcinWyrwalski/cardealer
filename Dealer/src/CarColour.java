public enum CarColour {

    SNOWY_WHITE (0),
    SANDY_YELLOW(0),
    BOTTLE_GREEN (0),
    PANITIES_PINK (800),
    DEEP_BLUE (800),
    CLASIC_SILVER (1200),
    FASTEST_RED (1900);

    private int price;

    CarColour(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
