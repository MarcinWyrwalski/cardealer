import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Apple {

     final String taste;
     final String colour;
     final int i;

    public Apple(String taste, String colour, int i) {

        this.taste = taste;
        this.colour = colour;
        this.i = i;
    }

    @Override
    public String toString() {
        return "Apple{" +
                "taste='" + taste + '\'' +
                ", colour='" + colour + '\'' +
                ", i=" + i + '\n'+
                '}';
    }

    public Apple desiredAppleOutput(Apple...appleVarg){
        List<Apple> apples = new ArrayList<>();
        for (int i=0; i<appleVarg.length; i++ ) {
            apples.add(appleVarg[i]);
        }
        apples.stream()
                .filter(apple -> apple.colour.equals("red")  && apple.taste.equals("good") && apple.i >3)
                .collect(Collectors.toList());

        return (Apple) apples;
    }
}
